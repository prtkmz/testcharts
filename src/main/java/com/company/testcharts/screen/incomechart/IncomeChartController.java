package com.company.testcharts.screen.incomechart;

import com.company.testcharts.config.ClickHouseStore;
import com.company.testcharts.entity.Income;
import io.jmix.core.DataManager;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@UiController("Incomechart")
@UiDescriptor("incomeChart.xml")
public class IncomeChartController extends Screen {
    @Autowired private CollectionContainer<Income> incomeDc;
    @Autowired DataManager dataManager;

    @Subscribe
    public void onInit(InitEvent event) {
        var list = dataManager.load(Income.class)
                              .all()
                              .list();
        incomeDc.setItems(list);
    }
}