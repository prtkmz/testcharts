package com.company.testcharts.dao;

import com.company.testcharts.entity.Income;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class IncomeDAO {

    private final DataSource dataSource;

    public IncomeDAO(@Qualifier("clickHouseDataSource") DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Income> getAllIncomes() {
        List<Income> incomes = new ArrayList<>();
        try (var conn = dataSource.getConnection()) {
            var preparedStatement = conn.prepareStatement("SELECT * FROM default.income");
            var resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                var income = new Income();
                income.setDate(resultSet.getDate("date"));
                income.setValue(resultSet.getInt("value"));
                incomes.add(income);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return incomes;
    }
}
