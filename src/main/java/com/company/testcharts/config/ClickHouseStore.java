package com.company.testcharts.config;

import com.company.testcharts.dao.IncomeDAO;
import io.jmix.core.DataStore;
import io.jmix.core.LoadContext;
import io.jmix.core.SaveContext;
import io.jmix.core.ValueLoadContext;
import io.jmix.core.entity.KeyValueEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Component("clickhouse_store")
@Scope(SCOPE_PROTOTYPE)
@RequiredArgsConstructor
public class ClickHouseStore implements DataStore {

    private final IncomeDAO incomeDAO;

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    @Nullable
    @Override
    public Object load(LoadContext<?> context) {
        return null;
    }

    @Override
    public List<Object> loadList(LoadContext<?> context) {
        return new ArrayList<>(incomeDAO.getAllIncomes());
    }

    @Override
    public long getCount(LoadContext<?> context) {
        return 0;
    }

    @Override
    public Set<?> save(SaveContext context) {
        return null;
    }

    @Override
    public List<KeyValueEntity> loadValues(ValueLoadContext context) {
        return null;
    }

    @Override
    public long getCount(ValueLoadContext context) {
        return 0;
    }
}
