package com.company.testcharts.config;

import io.jmix.core.metamodel.model.StoreDescriptor;
import org.springframework.stereotype.Component;

@Component("clickhouse_storeDescriptor")
public class ClickHouseStoreDescriptor implements StoreDescriptor {
    @Override
    public String getBeanName() {
        return "clickhouse_store";
    }

    @Override
    public boolean isJpa() {
        return false;
    }
}
