package com.company.testcharts.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
public class TestChartsConfiguration {

    @Bean
    @ConfigurationProperties("clickhouse.datasource")
    DataSourceProperties clickHouseDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "clickHouseDataSource")
    public DataSource getDataSource() {
        return clickHouseDataSourceProperties().initializeDataSourceBuilder().build();
    }
}
