package com.company.testcharts.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import lombok.Data;

import java.util.Date;

@Store(name = "clickhouse")
@JmixEntity
@Data
public class Income {
    private Date date;
    private Integer value;
}
